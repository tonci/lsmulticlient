Fork from https://github.com/dwm9100b/LsMultiClient.git by Dale Morrison

##LightSwitch 2013 - Multi-HTMLClient Project!##

In this post we will show you how to create a LightSwitch 2013 project that contains multiple HTMLClients.  It’s a pretty exciting development, as we can now create apps for specific areas, Admin vs. Worker, and all share the same data source within a single solution.

A side benefit is app payload.  We can break up large enterprise grade apps into multiple clients thus keeping the individual downloads to a reasonable size.

So… here we go!


- Open Visual Studio 2013
- Create a new LightSwitch Desktop Project
- Name:  LsMultiClient
- Solution name: LsMultiClient
- Create two test tables
	App1Item
	App2Item
- Set Forms Authentication
- Grant debug to SecurityAdministration
- Right click on the main LsMultiClient project
- Add a new Client, HTMLClient
- Name: App2
- Add screens to browse App2Items with add/edit 
- Open the default.htm file
- In the document.ready section, update the msls._run() to show the screen you'd like to have as the start screen for this app, msls._run("App2ItemsBrowse")
- Save all
- Right click on the LsMultiClient.App2 project
- Select Remove
- Right click again on the main LsMultiClient project
- Add a new Client, HTMLClient
- Name: App1
- Add screens to browse App1Items with add/edit
- Open the default.htm file
- In the document.ready section, update the msls._run() to show the screen you'd like to have as the start screen for this app, msls._run("App1ItemsBrowse")
- Save all
- Right click on the main LsMultiClient solution this time.
- Select Add, existing project
- Navigate to the LsMultiClient folder, find the LsMultiClient.App2 folder and select the project
- Save all
- Close your solution
- In a Windows explorer, navigate to your LsMultiClient solution folder.
- Locate and edit the LsMultiProject.lsxproj in a text editor
- Towards the bottom of the file, locate the  section.
- Copy the LsMultiClient.App1 section
- Paste it under the LsMultiClient.App1 section
- Change the 
ProjectReference Include="LsMultiClient.App1\LsMultiClient.App1.jsproj" to
ProjectReference Include="LsMultiClient.App2\LsMultiClient.App2.jsproj"
- Replace the text in the  tag to be LsMultiClient.App2
- Get the Project GUID from LsMultiProject.sln for your LsMultiClient.App2 project.
- Back in the LsMultiClient.lsxproj file
Under the new copied section for LsMultiClient.App2.jsproj
- Replace the GUID in the  tag with the App2 GUID from your LsMultiClient.sln file
- Save your files
- Back to Visual Studio 2013
- Open your LsMultiClient solution
- Select Build, Clean solution
- Select Build
- Run your app
- http://localhost:{port}/App1
- http://localhost:{port}/App2
- http://localhost:{port}/DesktopClient


Notes:

- Create your HTMLClients starting with your last one
- You can remove the DesktopClient once you are sure you are done adding HTMLClients
- Keep screen names globally unique
- Make sure you set your startup project


You can download an example project from our https://github.com/dwm9100b/LsMultiClient.git

We will be posting an LsCoreProject with multiple HTMLClients soon.